#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Oct 2021
@author: marie-claire.blache@inrae.fr, benoit.piegu@inrae.fr
"""

################################les modules##################################
from genericpath import exists
import os
import sys
import argparse
from contextlib import nullcontext
from pathlib import Path
from progress.bar import Bar
#from sys import setprofile
import cv2
import numpy as np



############################### #les paramètres ##################################

DEFAULT_STEP = 10
DEFAULT_SIZE = 224

CLASSES = { 0.95: "bon", 0.7: "verifier"}

parser = argparse.ArgumentParser(
    description="Applique une prédiction sur des flux video et écrit l'image suivant la prédiction",
    epilog="""
Recherche toutes les video *.MP4 présentes dans le répertoire spécifié par --dir RÉPERTOIRE.


Sur chacune des video applique le modèle donné en argument toutes les S images (--step S).
Pour appliquer le modèle, chacune des images est réduit à la taille NxN définis par --size N.
Chaque image originale avec une prédiction suffisante est écrite dans un répertoire ad hoc.
Ces répertoires sont créé là où sont les video lues


authors: marie-claire.blache@inrae.fr, benoit.piegu@inrae.fr
    """)
#parser.add_argument('video', nargs=1, help='nom de la vidéo')
parser.add_argument("--step", dest="step",
    choices=range(0, 60), default=DEFAULT_STEP, type=int,
    metavar="[0-60]",
    help="predict frame every step")
parser.add_argument("--dir", "-d", dest="dir", default=".",
                        help="dir where to execute the script")
parser.add_argument('--info', '-i', action='count',  default=0,
                        help="info)")
parser.add_argument("--size", '-s', dest="size",
                    choices=range(100, 600), default=DEFAULT_SIZE, type=int,
                    metavar="[10-600]",
                    help= "nombre de pixels en x et en y en entrée pour le modèle"
                    ". (default: %(default)i)")
parser.add_argument('model', nargs=1, help='file model .h5')
args = parser.parse_args()


#### script pour enregistrer les images dans un dossier au nom du mp4 ####
# définir le dossier
TOT_PATH = Path(args.dir)
#liste des fihcier mp4.
videoPaths = list(TOT_PATH.glob('**/*.MP4'))

# import des modules keras après avoir vérifier les arguments

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.applications import resnet50

if  'CUDA_VISIBLE_DEVICES' in os.environ and os.environ['CUDA_VISIBLE_DEVICES']!='-1':
    print("# cuda device", file=sys.stderr)
    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    print(f"# physical devices = {physical_devices}", file=sys.stderr)
    assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
else:
    # cpu
    physical_devices = tf.device("cpu:0")
    print("# tf.keras code in this scope will run on CPU", file=sys.stderr)

#load model
model = args.model[0]
print(f"# load model={model}", file=sys.stderr)
model = keras.models.load_model(model)


def predict(fvideo, model, classes, outdir, width=DEFAULT_SIZE, height=DEFAULT_SIZE,
nframe_max=None, step=1, nframe_per_sec=25, progress=True):

    fbase = os.path.splitext(os.path.basename(fvideo))[0]
    # frameRate = 0.04  #//it will capture image in each 0.04 second

    def classes_updatedir(directory, classes):
    # création des répertoires
        classepaths = {}
        for (threshold, classe) in classes.items() :
            classepath = Path.joinpath(directory, classe)
            classepaths[classe] = classepath
            if not Path.exists(classepath):
                print(f"[INFO] 'creating {classepath}' directory", file=sys.stderr)
                Path.mkdir(classepath, parents=True,  exist_ok=True)
        return classepaths

    def write_img(frame, iframe, pval, path):
        sec = round(iframe/nframe_per_sec, 2)
        nomimg = f'{fbase}_{str(sec)}s-{pval*100:.0f}.png'
        imgpath = Path.joinpath(path, nomimg)
        # print(f"# Image to write : {iframe} => {imgpath}", file=sys.stderr)
        cv2.imwrite(str(imgpath), frame)

    # définis & crée les chemins où seront écrits les images en fonction de leur prédiction
    classes_to_path = classes_updatedir(out_pred_dir, CLASSES)
    # les seuils doivent être trié dans l'ordre décroissant
    thresholds = sorted(classes.keys())
    thresholds.reverse()
    min_threshold = min(thresholds)
    # get total number of framestry:
    try:
        cap = cv2.VideoCapture(fvideo)
    except:
        print(f"#    Error: problem opening input stream for {fvideo}", file=sys.stderr)
        return
    total_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    if total_frames == 0:
        print(f"#    Error: {fvideo} has no frame", file=sys.stderr)
        return
    npreds = int(total_frames/step)
    img_batch = np.empty([npreds, args.size, args.size, 3], np.uint8)
    print(f"# fvideo={video} -- totFrames={total_frames} => step={step} npreds={npreds}", file=sys.stderr)
    if nframe_max is None:
        nframe_max = total_frames
    iframe = 0
    iimg = 0
    if progress:
        read_ctxt = Bar('# images reading', max=npreds,
                   suffix='%(index)d/%(max)d -- ETA=%(eta)ds -- elapsed=%(elapsed).1fs -- avg=%(avg).3fs')
    else:
        read_ctxt = nullcontext()

    with read_ctxt as bar:
        while(cap.isOpened() and iframe <= nframe_max):
            has_frames, frame = cap.read()
            if has_frames and iframe % step == 0:
                resized_img = cv2.resize(frame, dsize=(width, height),
                    interpolation=cv2.INTER_CUBIC)
                img_batch[iimg] = resized_img
                iimg += 1
                if iimg == npreds:
                    break
                if progress:
                    bar.next()
            iframe += 1
    cap.release()
    print("# prediction", file=sys.stderr)
    processed_img = resnet50.preprocess_input(img_batch)
    prediction = model.predict(processed_img)
    print("# filtering", file=sys.stderr)
    frame_to_write = {}
    for iframe in range(0, prediction.shape[0]):
        pval = prediction[iframe][1]
        if pval > min_threshold:
            for threshold in thresholds:
                if pval >= threshold:
                    frame_to_write[iframe] = (pval, classes[threshold])
                    break
    if len(frame_to_write):
        # for each resized and valid image, get original image
        nimg2write = len(frame_to_write.keys())
        cap = cv2.VideoCapture(fvideo)
        if progress:
            write_ctxt = Bar('# images to write', max=nimg2write,
                   suffix='%(index)d/%(max)d -- ETA=%(eta)ds -- elapsed=%(elapsed).1fs -- avg=%(avg).3fs')
        else:
            print(f"# write images ({nimg2write})", file=sys.stderr)
            write_ctxt = nullcontext()
        with write_ctxt as bar:
            for iframe in sorted(frame_to_write.keys()):
                (pval, classe) = frame_to_write[iframe]
                iframe *= step
                cap.set(cv2.CAP_PROP_POS_FRAMES, iframe)
                res, frame = cap.read()
                if res:
                    path = classes_to_path[classe]
                    write_img(frame, iframe, pval, path)
                if progress:
                    bar.next()
        cap.release()
    else:
        print(f"# there is no images to write", file=sys.stderr)
        


print("# predict ({} files)".format(len(videoPaths)), file=sys.stderr)
#pour chaque fichier mp4
for (i, video) in enumerate(videoPaths):
    #recuperer le nom
    video_dir = os.path.dirname(video)
    video_name = os.path.basename(video).split(".", maxsplit=1)[0]
    print(f"# {i+1}/{len(videoPaths)} video={video} => video_dir={video_dir} video_name={video_name}", file=sys.stderr)
    # créer les dossiers de sortie et mettre à jour classes pour la video en cours
    out_pred_dir = Path.joinpath(Path(video_dir), video_name)
    # prédiction
    predict(str(video), model, CLASSES, out_pred_dir, step=args.step, progress = bool(args.info))
print("# end of prediction")
